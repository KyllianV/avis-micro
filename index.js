const express = require('express')
const app = express()
const bodyParser = require('body-parser')

require('dotenv').config()

const avis = require('./routes/avis');

app.use(bodyParser.json())

app.use('/avis', avis);

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})