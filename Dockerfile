FROM node:8

RUN apt-get install git-core

WORKDIR /var/www/app

RUN git clone https://gitlab.com/KyllianV/avis-micro.git micro-avis
WORKDIR /var/www/app/micro-avis
RUN npm install

EXPOSE 3000

#CMD npm run prod
CMD [ "node", "index.js" ]
