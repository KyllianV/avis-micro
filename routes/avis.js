const router = require('express').Router();
const AvisDAO = require('../repositories/AvisDAO')

router.get('/', function (req, res) {

  let filter = {}
  if (req.query.idUser != null && req.query.idUser != '') {
    filter.idUser = req.query.idUser
  }
  if (req.query.idProduct != null && req.query.idProduct != '') {
    filter.idProduct = req.query.idProduct
  }

  const avisDao = new AvisDAO()
  avisDao.get(filter).then((response) => {
    res.status(200).send(response)
  }).catch(err => {
    res.sendStatus(500)
  })
})

router.post('/', function (req, res) {
  let avis = {
    "text": req.body.text,
    "idUser": req.body.user,
    "idProduct": req.body.product,
    "rank": req.body.rank
  }

  const avisDao = new AvisDAO()
  avisDao.create(avis).then(() => {
    res.sendStatus(201)
  }).catch(err => {
    res.sendStatus(500)
  })
})

router.put('/:idAvis', function (req, res) {
  let avis = {
    "idAvis": req.params.idAvis,
    "text": req.body.text,
    "idUser": req.body.user,
    "idProduct": req.body.product,
    "rank": req.body.rank
  }

  const avisDao = new AvisDAO()
  avisDao.update(avis).then(() => {
    res.sendStatus(200)
  }).catch(err => {
    res.sendStatus(500)
  })
})

router.delete('/:idAvis', function (req, res) {

  const avisDao = new AvisDAO()
  avisDao.delete(req.params.idAvis).then(() => {
    res.sendStatus(204)
  }).catch(err => {
    res.sendStatus(500)
  })
})

module.exports = router;
