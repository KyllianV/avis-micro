const mongoose = require('mongoose');

const Avis = mongoose.model('Avis', {
  text: String,
  idUser: Number,
  idProduct: Number,
  rank: Number
});

module.exports = Avis;
