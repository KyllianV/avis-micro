const mongoose = require('mongoose');
mongoose.connect('mongodb://mongo:27017/avis');

const Avis = require('../models/Avis')

class AvisDAO {
  get(filter) {
    return Avis.find(filter)
  }

  create(avis) {
    const newAvis = new Avis(avis)
    return newAvis.save()
  }

  update(avis) {
    return Avis.findByIdAndUpdate(avis.idAvis, avis)
  }

  delete(idAvis) {
    return Avis.findByIdAndRemove(idAvis)
  }
}

module.exports = AvisDAO